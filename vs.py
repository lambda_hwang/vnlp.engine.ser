from pandas.core.algorithms import mode
import os
import random
import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import gc
from tqdm import tqdm



class VoiceClassifier(nn.Module):
    
    def __init__(self, audio_dim = 13, max_len = 300, 
                 num_kernels = [200,100,50,25],
                 kernel_sizes = [(7,13),(5,100),(3,50),(2,25)]):
        
        super(VoiceClassifier,self).__init__()
        self.convs = nn.ModuleList([])
        self.norms = nn.ModuleList([])
        
        for i in range(0,len(kernel_sizes)):
            self.convs.append(nn.Conv2d(in_channels = 1,
                                        out_channels = num_kernels[i],
                                        kernel_size = kernel_sizes[i],
                                        stride = 2))
            self.norms.append(nn.BatchNorm2d(num_kernels[i]))
        
        self.dropout = nn.Dropout(0.2)
        self.fc1 = nn.Linear(204, 2)  # Determine in runtime
        self.softmax = nn.Softmax(dim=1)

        gc.collect()
        
    def forward(self, x):
        # x = [N, max_len, 512]
        
        for conv, norm in zip(self.convs, self.norms):
            x = x.unsqueeze(1) # x = [N, num_channels, max_len, 512]
            x = F.relu(norm(conv(x))) # x = [N, num_kernel, K, 1]
            x = x.squeeze(3) # x = [N, num_kernel, K]
            x = x.permute(0, 2, 1) # x = [N, K, num_kernel]
            x = F.max_pool1d(x, 2) # x = [N, K, num_kernel / 2] # x = F.avg_pool1d(x, 2)
            x = self.dropout(x)
        
        x = x.reshape(-1, x.size(1) * x.size(2))
        logit = self.fc1(x)
        logit = self.softmax(logit)

        gc.collect()
        
        return logit



def over_sample(data, multifold):
    one_indexes = [i for i in range(len(data)) if data[i][1]==1]
    one_rows = [data[i] for i in one_indexes]
    data_ovsp = data.copy()
    for i in range(multifold):
        data_ovsp.extend(one_rows)
    random.shuffle(data_ovsp)
    return data_ovsp

def validate_model(model, criterion, test_loader):
    y_pred = []
    y_true = []
    valid_loss = []
    
    for features, labels in test_loader:
        features = Variable(features).cuda()
        labels = labels.cuda()
        outputs = model(features)
        
        #_, predicted = torch.max(outputs.data, 1)
        predicted = [1 if outputs.data[i, 1] > 0.5 else 0 for i in range(outputs.data.shape[0])]
        y_pred.extend(predicted)#.cpu().tolist())
        y_true.extend(labels.cpu().tolist())
        
        loss_ = criterion(outputs.data, labels)
        valid_loss.append(loss_.data.item())
    
    accuracy = accuracy_score(y_true, y_pred) * 100
    precision = precision_score(y_true, y_pred) * 100
    recall = recall_score(y_true, y_pred) * 100
    f1 = f1_score(y_true, y_pred) * 100
    l = sum(valid_loss) / len(valid_loss)

    return accuracy, recall, precision, f1, l

def make_dataloader():
    # df_train = pd.read_csv('training_dataset/train_dataset_telesale.csv', delimiter="\t", header=None)
    # X = []; y = df_train[3].to_list()
    df_train = pd.read_csv('training_dataset/tabular_train_dataset_telesale+genesys.csv')
    X = []; y = df_train['label'].to_list()

    for p in tqdm(df_train['wav_file']):
        p_split = p.split('/')
        p_split[2] = p_split[2].replace('wav', 'mfcc')
        if '78h' in p_split[0]: p_split[2] = p_split[2].split('__')[1]
        p_split[1] = p_split[0]
        p_split[0] = 'training_dataset'
        p = '/'.join(p_split)
        features = torch.load(p)
        X.append(features)
    print('Train data loaded! (100%)', 'X:', len(X), X[0].shape, ' | y:', len(y), ' | negative_count:', sum(y))

    train_dataset = [(xx, yy) for xx, yy in zip(X, y)]
    train_size = len(train_dataset)

    df_test = pd.read_csv('training_dataset/tabular_test_dataset_telesale+.csv')
    X = []; y = df_test['label'].to_list()

    for p in tqdm(df_test['wav_file']):
        p_split = p.split('/')
        p_split[2] = p_split[2].replace('wav', 'mfcc')
        p_split[1] = p_split[0]
        p_split[0] = 'training_dataset'
        p = '/'.join(p_split)
        features = torch.load(p)
        X.append(features)
    print('Test data loaded! (100%)', 'X:', len(X), X[0].shape, ' | y:', len(y), ' | negative_count:', sum(y))

    test_dataset = [(xx, yy) for xx, yy in zip(X, y)]
    
    ### CLASS-WEIGHTING
    class_1_count = sum([xy[1] for xy in train_dataset])
    weights = 1. / np.array([train_size - class_1_count, class_1_count]) * 2
    class_weights = torch.FloatTensor(weights).cuda()

    batch_size = 128
    n_iters = 20000
    n_epochs = int(n_iters / (train_size / batch_size)); print('Number of epochs:', n_epochs)

    train_loader = torch.utils.data.DataLoader(train_dataset, 
                                               batch_size,
                                               num_workers=8,
                                               shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size)
    return train_loader, test_loader, n_epochs, class_weights


if __name__ == '__main__':
    last_checkpoint = None # torch.load('checkpoints/checkpoint30.pt')

    train_loader, test_loader, n_epochs, class_weights = make_dataloader()
    
    model = VoiceClassifier()
    if last_checkpoint!=None:
        model.load_state_dict(last_checkpoint['model_state_dict'], map_location=torch.device('cuda'))
    model.cuda()
    
    criterion = nn.CrossEntropyLoss(weight=class_weights)
    optimizer = torch.optim.Adam(model.parameters(), lr=3e-5, weight_decay=1e-5) # best: 3e-5, 2e-5
    
    if last_checkpoint!=None:
        optimizer.load_state_dict(last_checkpoint['optimizer_state_dict'])
        first_epoch = last_checkpoint['epoch'] + 1
        iter = last_checkpoint['iter']
        best_f1 = last_checkpoint['best_f1']; best_loss = last_checkpoint['best_loss']
        write_type = 'a'
    else:
        first_epoch = 0
        iter = 0
        best_f1 = 0; best_loss = 1
        write_type = 'w'
    
    train_loss_log = []; valid_loss_log = []
    running_loss = 0.0
    chkpt_dir = './checkpoints'

    for epoch in range(first_epoch, n_epochs):
        print('EPOCH', epoch)
        for features, labels in train_loader:
            features = Variable(features.cuda())
            labels = Variable(labels.cuda())
            outputs = model(features)
            
            optimizer.zero_grad()
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            
            running_loss += loss.data.item()
            iter += 1

            if iter % 100 == 0:
                ac, re, pr, f1, l = validate_model(model, criterion, test_loader)
                train_loss_log.append(running_loss * 0.01)
                valid_loss_log.append(l)
                print('[{}] {:.3f} - {:.3f} ~ {:.3f} | {:.2f}% | {:.2f}% | {:.2f}% | {:.2f}% ~ {:.2f}%'.format(iter, train_loss_log[-1], valid_loss_log[-1], best_loss, ac, re, pr, f1, best_f1))
                
                if f1 > best_f1:
                    best_f1 = f1
                    torch.save(model.state_dict(), os.path.join(chkpt_dir, 'checkpoint_best_f1.pt'))
                    print('\n*** Checkpoint best f1 saved! ***\n')
                if l < best_loss:
                    best_loss = l
                    torch.save(model.state_dict(), os.path.join(chkpt_dir, 'checkpoint_best_loss.pt'))
                    print('\n*** Checkpoint best loss saved! ***\n')
                
                with open('train_loss_log.txt', write_type) as f:
                    for l in train_loss_log:
                        f.write(str(l) + '\n')
                with open('valid_loss_log.txt', write_type) as f:
                    for l in valid_loss_log:
                        f.write(str(l) + '\n')
                
                running_loss = 0

        #torch.save(model.state_dict(), os.path.join(chkpt_dir, f'checkpoint{epoch}.pt'))
        torch.save({
            'epoch': epoch,
            'iter': iter,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_f1': best_f1,
            'best_loss': best_loss,
            }, os.path.join(chkpt_dir, f'checkpoint{epoch}.pt'))
    
    print('Training finished! || Best F1:', best_f1, '|| Best valid loss:', best_loss)
    
    torch.save(model.state_dict(), os.path.join(chkpt_dir, 'checkpoint_last.pt'))